#!/usr/bin/env bash

clear

#set -eo pipefail

if [ ! -f "index.php" ]; then
  echo 'Please run this script from "drupal8" directory.'
  exit 1;
fi

if [ $(pwd) = '/var/www/html' ]; then
  echo 'Please run this script only under LOCAL enviroment.'
  exit 1;
fi

get_path() {
  # $1 : file path.
  if [ -d "$(dirname "$1")" ]; then
    echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
  fi
}

$(echo $(pwd)"/scripts/project-branch");

## #############################################################################
echo 'Reinstalling ...'

DB_FILE=$(get_path "$1")

if [ $# -ne 1 ]
then
	echo "Usage: $0 {DB_FILE}"
	exit 1
fi

# ##############################################################################
PROJECT="$(cat "./.project-name" | tr -d '[:space:]')";
DOCKER=$(get_path "$(pwd)/ci");
DOCKER_YML="${DOCKER}/docker-compose.yml";
DOCKER_CMD="docker-compose -f ${DOCKER_YML} -p ${PROJECT} exec php sh -c";

## ##############################################################################
echo "Drop all tables"
$DOCKER_CMD 'drush sql-drop -y';

echo "Import new DB"
DB_TEMP_FILE=$(mktemp "${DOCKER}/dump/reinstall.XXXXXX.sql")
if file --mime-type "$DB_FILE" | grep -q gzip$; then
  gunzip -c $DB_FILE > $DB_TEMP_FILE;
else
  cat $DB_FILE > $DB_TEMP_FILE;
fi

$DOCKER_CMD "$(echo "drush sqlc < /tmp/db/$(basename "$DB_TEMP_FILE")")" > /dev/null 2>&1

rm -f $DB_TEMP_FILE

## #############################################################################
echo "Clear cache tables"
CACHE_TABLES=( $( $DOCKER_CMD "$(echo "drush sqlq \"SHOW TABLES LIKE 'cache\_%';\"")" | sed 's/\x0D$//' ) )
TRUNCATE_CACHE_TABLES=$(printf "TRUNCATE %s; " "${CACHE_TABLES[@]}")
$DOCKER_CMD "$(echo "drush sqlq '${TRUNCATE_CACHE_TABLES}'")" > /dev/null 2>&1

## #############################################################################
if [ -d ./sites/gojysk.com/files ]; then
  echo "Clean compiled files"
  sudo rm -fr ./sites/gojysk.com/files/php
  sudo rm -fr ./sites/gojysk.com/files/js
  sudo rm -fr ./sites/gojysk.com/files/css
  sudo rm -fr ./sites/gojysk.com/files/languages
  sudo chmod -R -x+X ./sites/gojysk.com/files
fi

## #############################################################################
echo "CI/CD"
echo "$(date +"%Y%m%d%H%M%S")1" > ./sites/gojysk.com/hash_salt.txt

#$DOCKER_CMD './vendor/bin/drush rr       --uri=go.jysk.local'
$DOCKER_CMD './vendor/bin/drush updb -y  --uri=go.jysk.local'
$DOCKER_CMD './vendor/bin/drush cr       --uri=go.jysk.local'
$DOCKER_CMD './vendor/bin/drush config-split-import -y --uri=go.jysk.local'
$DOCKER_CMD './vendor/bin/drush cr       --uri=go.jysk.local'
$DOCKER_CMD './vendor/bin/drush updb -y  --uri=go.jysk.local'
$DOCKER_CMD './vendor/bin/drush cr       --uri=go.jysk.local'

echo "$(date +"%Y%m%d%H%M%S")2" > ./sites/gojysk.com/hash_salt.txt
$DOCKER_CMD "$(echo "./vendor/bin/drush sqlq '${TRUNCATE_CACHE_TABLES}'")" > /dev/null 2>&1
$DOCKER_CMD './vendor/bin/drush rebuild';

## #############################################################################
echo "Re-install DB is DONE!"

xdg-open "$($DOCKER_CMD 'drush uli --no-browser --uid=1')" > /dev/null 2>&1

exit 0
