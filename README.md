# Пример разворачивания проекта project_name
* Настраиваем поддомен (или домен) на хостинге
* Подключаемся через SSH
* Удаляем папку на сервере проэкта
```bash
rm -rf project_name/
```
* Копируем инсталяционный профайл в папку с названием проекта (сабдомена project_name) или www если хотим развернуть на основном сайте.
```bash
git clone git@bitbucket.org:zebra-lab/zebralab-base-profile.git project_name
```
* Запускаем композер
```bash
cd project_name
composer install
composer update
```
* Удаляем историю Git и отправляем все на новый сервер
```bash
rm -rf .git
git init
git add .
git commit -m "Initial commit"
git remote add origin git@bitbucket.org:team-name/project_name.git
git push -u --force origin develop
```
* Дальше просто устанавливаем проект через Web интерфейс.

# Установка новых модулей
```bash
composer require drupal/devel:~1.0
drush en -y devel
```

# Экспорт текущей конфигурации в yml конфиг файлы.
```bash
drush config-export -y
```

# Commit и push изменений в репозиторий.
```bash
git add.
git commit -m "My massage"
git push
```